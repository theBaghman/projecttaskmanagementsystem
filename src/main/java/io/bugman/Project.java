package io.bugman;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Project {
    private int id;

    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private List<Task> tasks;
    public Project(int id, String name, String description, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
