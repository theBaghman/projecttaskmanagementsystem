package io.bugman;

import java.util.Date;

public class Task {
    private int id;
    private String title;
    private String description;

    private String status;

    private int priority;
    private Date dueDate;
    private User assignee;
    public Task(int id, String title, String description, String status, int priority, Date dueDate, User assignee) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.priority = priority;
        this.dueDate = dueDate;
        this.assignee = assignee;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public int getPriority() {
        return priority;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public User getAssignee() {
        return assignee;
    }
}
